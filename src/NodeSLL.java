/**
 * This class represents the way one element in SLL is stored like
 * Node is Single Linked
 * It contains value of type T and link to next node
 *
 * @param <T> This class can work with any data type
 */
public class NodeSLL<T> {
    private NodeSLL next;
    private T value;

    //Constructor
    NodeSLL(T value) {
        this.value = value;
    }

    public NodeSLL getNext() {
        return this.next;
    }

    //getters and setters
    public void setNext(NodeSLL next) {
        this.next = next;
    }

    public T getValue() {
        return this.value;
    }
}
