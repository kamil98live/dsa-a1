import java.util.Random;

/**
 * This class is a template for polygons on Euclidian plane
 * An object of this class represents a polygon with vertexes (p1,p2,...,pn) which are objects of class Point
 * Each polygon has its vertexes and size - number of vertexes or segments
 * minX,maxX,minY,maxY determine the coordinates of rectangle around the polygon
 */
public class Polygon {
    private Point vertexes[];
    private double minX, maxX, minY, maxY;

    /**
     * Constructor for Polygon class
     *
     * @param inputDoubles input coordinates of vetexes {x1,y1,x2,y2,...}
     * @param begin        first index of input array
     * @param end          last index of input array
     */
    Polygon(double[] inputDoubles, int begin, int end) {
        minX = minY = Double.MAX_VALUE;
        maxX = maxY = Double.MIN_VALUE;

        vertexes = new Point[(end - begin + 1) / 2];

        for (int i = begin, pos = 0; i <= end; i += 2, pos++) {
            vertexes[pos] = new Point(inputDoubles[i], inputDoubles[i + 1]);
            minX = Math.min(minX, vertexes[pos].getX());
            maxX = Math.max(maxX, vertexes[pos].getX());
            minY = Math.min(minY, vertexes[pos].getY());
            maxY = Math.max(maxY, vertexes[pos].getY());
        }
    }

    /**
     * This method is to check if a point is inside of this polygon
     *
     * @param point
     * @return
     */
    public boolean insidePolygon(Point point) {
        //if point belongs to polygon's bound the answer is true
        for (int i = 0; i < this.getSize(); i++) {
            if (Geometry.pointIsOnSegment(point, vertexes[i], vertexes[(i + 1) % getSize()])) {
                return true;
            }
        }
        //else we count how many times ray(actually segment with points(point,endA)) intersects the polygon
        int count = 0;
        Point endA = new Point(Double.MAX_VALUE, maxY + 1.0);
        for (int i = 0; i < this.getSize(); i++) {
            //if ray intersects the segment of the polygon and not collinear with it we increase count
            if (Geometry.intersects(point, endA, vertexes[i], vertexes[(i + 1) % getSize()]) &&
                    !Geometry.areCollinear(point, endA, vertexes[i], vertexes[(i + 1) % getSize()])) {
                count++;
            }
        }
        //if count is odd, the point is inside the polygon
        if (count % 2 == 1)
            return true;
        else
            return false;
    }

    /**
     * This method is to calculate area of the polygon
     * Implements Monte-Carlo method
     *
     * @param accuracy the accuracy of calculations
     * @return double value - area of the polygon
     */
    public double calculateArea(double accuracy) {
        double previousArea = 0;
        double currentArea = Double.MAX_VALUE;
        Random random = new Random();
        int insidePoints = 0;
        int allPoints = 0;
        while (Math.abs(currentArea - previousArea) >= accuracy) {
            for (int i = 0; i < 10000; i++) {
                Point point = new Point(random.nextDouble() * (maxX - minX) + minX, random.nextDouble() * (maxY - minY) + minY);

                if (this.insidePolygon(point))
                    insidePoints++;
                allPoints++;
            }
            previousArea = currentArea;
            currentArea = (maxX - minX) * (maxY - minY) * ((double) insidePoints / (double) allPoints);
        }
        return currentArea;
    }

    /**
     * @return - returns size of the polygon
     */
    public int getSize() {
        return vertexes.length;
    }
}
