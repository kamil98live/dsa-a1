import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Problem1 {
    /**
     * This method parses the string for doubles
     *
     * @param inputString input
     * @return an array of doubles
     */
    public static double[] parseDoubles(String inputString) {

        inputString = inputString.replaceAll("\\{", "");
        inputString = inputString.replaceAll("\\}", ",");
        inputString = inputString.replaceAll("\\(", "");
        inputString = inputString.replaceAll("\\)", "");
        inputString = inputString.replaceAll(",", " ");

        String[] inputStringArray = inputString.split(" ");
        double[] doubles = new double[inputStringArray.length];
        for (int i = 0; i < doubles.length; i++) {
            doubles[i] = Double.parseDouble(inputStringArray[i]);
        }
        return doubles;
    }

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("input.txt"));

        //reading input
        String inputString = scanner.nextLine();
        inputString += scanner.nextLine();
        scanner.close();

        //parsing inputString to an array of doubles
        double doubles[] = parseDoubles(inputString);

        //creating inputPoint
        Point inputPoint = new Point(doubles[doubles.length - 2], doubles[doubles.length - 1]);

        //creating a Polygon class object
        Polygon polygon = new Polygon(doubles, 0, doubles.length - 3);

        //Writing answer to output
        FileWriter out = new FileWriter(new File("output.txt"));
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        decimalFormat.setMaximumFractionDigits(1);
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        out.write(polygon.insidePolygon(inputPoint) ? "Yes" : "No");
        out.write("\n");
        out.write(decimalFormat.format(polygon.calculateArea(1e-5)));
        out.flush();
        out.close();
    }
}
