import java.util.NoSuchElementException;

/**
 * Implementation of LinkedStack
 *
 * @param <T> - any type of elements in stack
 */
public class LinkedStack<T> implements MyStack {
    //There is link to the top of the stack, size variable and constructor
    private NodeSLL top;
    private int size;

    public LinkedStack() {
        this.size = 0;
    }

    /**
     * This method returns the size of the stack
     *
     * @return size of the stack
     */
    @Override
    public int getSize() {
        return size;
    }

    /**
     * This method is to check if the stack is empty
     *
     * @return boolean value
     */
    @Override
    public boolean isEmpty() {
        return getSize() == 0;
    }

    /**
     * This method returns the element from the top of the stack
     *
     * @return element from the top
     */
    @Override
    public T top() {
        if (this.isEmpty()) throw new NoSuchElementException("There are no elements in empty stack");
        else return (T) top.getValue();
    }

    /**
     * This method pushes element to the top of the stack
     * If stack is empty we initialize the first element
     * else just add element and link it with previous top
     * update top
     *
     * @param value value to be pushed
     */
    @Override
    public void push(Object value) {
        NodeSLL node = new NodeSLL(value);
        if (isEmpty()) {
            top = node;
        } else {
            node.setNext(top);
            top = node;
        }
        size++;
    }

    /**
     * This method deletes and returns element from the top
     * We take element from the top and update the top
     * If stack is empty, we return null
     *
     * @return element from the top or null
     */
    @Override
    public Object pop() {
        if (this.isEmpty()) throw new NoSuchElementException("There are no elements in empty stack");
        else {
            NodeSLL node = top;
            top = top.getNext();
            size--;
            return (T) node.getValue();
        }
    }
}
