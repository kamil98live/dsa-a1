/**
 * This class contains static methods to work with tokens
 * Tokens always represented by String in-built type
 * A token is operator "+", "-", "*", "/", parenthesis "(", ")" or number (positive integer or double)
 */
public final class Token {
    /**
     * Constructor is private to prevent creation objects of this class
     * Token is always represented by String in-built type
     */
    private Token() {
    }

    /**
     * Method is to get priority of an operator
     *
     * @param token input operator string
     * @return priority integer value
     */
    public static int getPriority(String token) {
        if (token.equals("+")) return 1;
        if (token.equals("-")) return 1;
        if (token.equals("*")) return 2;
        if (token.equals("/")) return 2;
        else throw new IllegalArgumentException("Illegal argument. This is not operator or bracket");
    }

    /**
     * Method to check if token is operator
     *
     * @param value input token string
     * @return boolean value, true if token is operator
     */
    public static boolean isOperator(String value) {
        if (value.equals("+") || value.equals("-") || value.equals("*") || value.equals("/")) return true;
        else return false;
    }

    /**
     * Method to check if token is number (positive integer or double)
     *
     * @param value input token string
     * @return boolean value, true if token is number
     */
    public static boolean isNumber(String value) {
        int dots = 0;
        if (!Character.isDigit(value.toCharArray()[0])
                || !Character.isDigit(value.toCharArray()[value.length() - 1]))
            return false;

        for (int i = 1; i < value.length() - 1; i++) {
            if (value.toCharArray()[i] == '.')
                dots++;
            else if (!Character.isDigit(value.toCharArray()[i]))
                return false;
        }
        if (dots > 1)
            return false;
        else return true;
    }

    /**
     * Method to get(parse) tokens from input string
     * We go char by char through the input string
     *
     * @param input input string, not parsed
     * @return queue, sequence of tokens found in input string
     */
    public static MyQueue getTokens(String input) throws Exception {
        input = input.replaceAll(" ", "");
        MyQueue output = new LinkedQueue();
        //this string is for temporary storing number symbols (chars)
        String temp = "";
        for (int i = 0; i < input.length(); i++) {
            char c = input.toCharArray()[i];
            String s = Character.toString(c);
            if (Token.isOperator(s) || s.equals("(") || s.equals(")")) {
                if (!temp.isEmpty()) {
                    if (!Token.isNumber(temp))
                        throw new Exception("Illegal token format. This is not operator, bracket or number: " + temp);
                    else {
                        output.enqueue(temp);
                        temp = "";
                    }
                }
                output.enqueue(s);
            } else {
                temp += s;
            }
        }
        if (!temp.isEmpty()) {
            if (!Token.isNumber(temp))
                throw new Exception("Illegal token format. This is not operator, bracket or number: " + temp);
            else output.enqueue(temp);
        }
        return output;
    }

    /**
     * Method to count the result of binary operation
     *
     * @param a        operand right (double)
     * @param b        operand left (double)
     * @param operator operator token
     * @return result of operation (double)
     */
    public static double count(double a, double b, String operator) {
        if (operator.equals("+")) return b + a;
        if (operator.equals("-")) return b - a;
        if (operator.equals("*")) return b * a;
        if (operator.equals("/")) return b / a;
        else throw new IllegalArgumentException("Incorrect input");
    }
}
