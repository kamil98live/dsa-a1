public interface MyQueue<T> {
    /**
     * adds an element to the end
     */
    void enqueue(T value);

    /**
     * deletes an element from the beginning
     */
    T dequeue();

    /**
     * returns an element from the beginning
     */
    T peek();

    /**
     * returns the size of the queue
     */
    int getSize();

    /**
     * returns true if the queue is empty
     */
    boolean isEmpty();
}
