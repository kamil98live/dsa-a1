/**
 * This class is a template for points on Euclidian plane
 * An object of this class represents a point with x and y coordinates
 */
class Point {
    private double x;
    private double y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
