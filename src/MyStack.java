public interface MyStack<T> {
    /**
     * returns the size of the stack
     */
    int getSize();

    /**
     * returns true if the stack is empty
     */
    boolean isEmpty();

    /**
     * returns the element on the top
     */
    T top();

    /**
     * adds new element to the top of the stack
     */
    void push(T value);

    /**
     * deletes the element from the tiop of the stack
     */
    T pop();
}
