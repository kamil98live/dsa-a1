import java.util.NoSuchElementException;

/**
 * Implementation of LinkedQueue
 *
 * @param <T> - any type of elements in queue.
 */
class LinkedQueue<T> implements MyQueue {

    /**
     * there are always head and tail nodes which represent the beginning and the end of the queue
     * head and tail equal to null by default
     * size equals to 0 initially
     */
    private NodeSLL head;
    private NodeSLL tail;
    private int size;

    /**
     * Constructor for class LinkedQueue
     */
    LinkedQueue() {
        this.size = 0;
    }

    /**
     * This method implements adding element to the end of the queue
     * <p>
     * If the size of the queue equals to 0 or, in other words, the queue is empty,
     * the element which is being added will be the only one element in the queue
     * This element is head and tail in the queue
     * By the way we link the same node to itself, so it makes easy to add next elements.
     * Else we just add the element to the end, link it with previous element and set new tail
     *
     * @param value - is the element of type T that is being added
     */
    @Override
    public void enqueue(Object value) {
        if (size == 0) {
            head = tail = new NodeSLL((T) value);
            head.setNext(tail);
        } else {
            tail.setNext(new NodeSLL<>((T) value));
            tail = tail.getNext();
        }
        size++;
    }

    /**
     * This method deletes an element from the beginning of the queue
     * If there are more than 1 element in the queue
     * we move head to the next element
     * If the queue is empty return null
     *
     * @return - returns the value of deleted element or null
     */
    @Override
    public T dequeue() {
        if (this.isEmpty()) throw new NoSuchElementException("Dequeue from empty queue is impossible");
        else {
            NodeSLL node = head;
            if (size > 1) {
                head = head.getNext();
            }
            size--;
            return (T) node.getValue();

        }
    }

    /**
     * This method returns element on head of the queue
     *
     * @return element of the head
     */
    @Override
    public T peek() {
        if (this.isEmpty()) throw new NoSuchElementException("Peek from empty queue is impossible");
        else return (T) head.getValue();
    }

    /**
     * @return size of the queue
     */
    public int getSize() {
        return size;
    }

    /**
     * @return true if queue is empty
     */
    @Override
    public boolean isEmpty() {
        return (getSize() == 0);
    }
}
