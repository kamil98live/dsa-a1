import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Problem2 {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("input.txt"));
        //Firstly we read input to the string called inS
        //Then we split it by " " so we can get the last part which is integer and equals to k
        String inS = scanner.nextLine();
        scanner.close();

        String r[] = inS.split(" ");
        int k = Integer.parseInt(r[r.length - 1]);

        //We put every element from input except the last into the queue
        MyQueue q = new LinkedQueue();
        for (int i = 0; i < r.length - 1; i++) {
            q.enqueue(r[i]);
        }

        //While there are more than 1 person in the circle we skip k-1 people and execute thee kth person
        //To skip one person we just re-add him into the queue
        //To execute we just drop it from the queue
        while (q.getSize() > 1) {
            //We avoid re-adding the whole queue by this
            int y = (k - 1) % q.getSize();
            for (int i = 0; i < y; i++)
                q.enqueue(q.dequeue());
            q.dequeue();
        }

        //We write out the only one element remaining in the queue - the man who is given freedom
        FileWriter out = new FileWriter(new File("output.txt"));
        out.write(q.dequeue().toString());
        out.flush();
        out.close();

    }
}
