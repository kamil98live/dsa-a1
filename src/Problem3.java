import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("input.txt"));
        String inputString = scanner.nextLine();
        scanner.close();

        DecimalFormat df = new DecimalFormat("#.#");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        df.setRoundingMode(RoundingMode.HALF_UP);

        //If input is wrong we catch exception
        String output;
        try {
            double result = SYAlgo.countRPN(SYAlgo.infixToPostfix(Token.getTokens(inputString)));
            output = df.format(result);
        } catch (Exception e) {
            output = "ERROR";
        }

        FileWriter out = new FileWriter(new File("output.txt"));
        out.write(output);
        out.flush();
        out.close();

    }
}
