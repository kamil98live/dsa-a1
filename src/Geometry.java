/**
 * Geometry methods
 */
public class Geometry {
    /**
     * This method is to check if two vectors are collinear
     *
     * @param startA starting point of vector A
     * @param endA   ending point of vector A
     * @param startB starting point of vector B
     * @param endB   starting point of vector B
     * @return boolean value
     */
    public static boolean areCollinear(Point startA, Point endA, Point startB, Point endB) {
        return (vectorsProduct(startA, endA, startB, endB) == 0);
    }

    /**
     * This method is to check if two vectors have intersection point
     *
     * @param startA starting point of vector A
     * @param endA   ending point of vector A
     * @param startB starting point of vector B
     * @param endB   starting point of vector B
     * @return boolean value
     */
    public static boolean intersects(Point startA, Point endA, Point startB, Point endB) {
        return (vectorsProduct(startA, endA, startA, startB) * vectorsProduct(startA, endA, startA, endB) <= 0 &&
                vectorsProduct(startB, endB, startB, startA) * vectorsProduct(startB, endB, startB, endA) <= 0);
    }

    /**
     * This method is to check if point is on segment
     *
     * @param point  input point
     * @param startA starting point of segment
     * @param endA   ending point of segment
     * @return boolean value
     */
    public static boolean pointIsOnSegment(Point point, Point startA, Point endA) {
        return (areCollinear(startA, point, startA, endA) &&
                point.getY() <= Math.max(startA.getY(), endA.getY()) &&
                point.getY() >= Math.min(startA.getY(), endA.getY()) &&
                point.getX() <= Math.max(startA.getX(), endA.getX()) &&
                point.getX() >= Math.min(startA.getX(), endA.getX()));
    }

    /**
     * This method is to calculate vector product of two vectors
     *
     * @param startA starting point of vector A
     * @param endA   ending point of vector A
     * @param startB starting point of vector B
     * @param endB   starting point of vector B
     * @return double value
     */
    public static double vectorsProduct(Point startA, Point endA, Point startB, Point endB) {
        return ((endA.getX() - startA.getX()) * (endB.getY() - startB.getY()) - (endB.getX() - startB.getX()) * (endA.getY() - startA.getY()));
    }
}
