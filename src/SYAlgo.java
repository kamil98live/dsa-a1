/**
 * Shunting-Yard Algorithm class
 * This class contains static method for this algorithm
 */
public final class SYAlgo {
    /**
     * Constructor is private to prevent creation objects of this class
     */
    private SYAlgo() {
    }

    /**
     * Method to convert token sequence from infix notation to postfix notation (RPN)
     *
     * @param buffer queue of input token sequence in infix notation
     * @return queue of token sequence in postfix notation
     */
    public static MyQueue infixToPostfix(MyQueue buffer) {
        MyQueue output = new LinkedQueue();
        MyStack stack = new LinkedStack();
        while (!buffer.isEmpty()) {
            String token = (String) buffer.dequeue();
            if (Token.isNumber(token))
                output.enqueue(token);
            if (Token.isOperator(token)) {
                while (!stack.isEmpty() && Token.isOperator((String) stack.top()))
                    if (Token.getPriority(token) <= Token.getPriority((String) stack.top()))
                        output.enqueue(stack.pop());
                stack.push(token);
            }
            if (token.equals("("))
                stack.push(token);
            if (token.equals(")")) {
                while (!(((String) stack.top()).equals("(")))
                    output.enqueue(stack.pop());
                stack.pop();
            }
        }
        while (!stack.isEmpty()) {
            if (((String) stack.top()).equals("(") || ((String) stack.top()).equals(")"))
                throw new IllegalArgumentException("Input is incorrect");
            output.enqueue(stack.pop());
        }
        return output;
    }

    /**
     * Method to count the result of expression in postfix notation represented by queue of token sequence
     */
    public static double countRPN(MyQueue input) {
        MyStack stack = new LinkedStack();
        while (!input.isEmpty()) {
            String token = (String) input.dequeue();
            if (Token.isNumber(token))
                stack.push(token);
            else
                stack.push(String.valueOf(Token.count(Double.parseDouble((String) stack.pop()), Double.parseDouble((String) stack.pop()), token)));
        }
        return Double.parseDouble((String) stack.pop());
    }
}
